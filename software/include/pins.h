/*
 * This file has a bunch of directives (aliases)
 * for pins.
 *
 * Example:
 * #define LED1     P1_0
 *
 * Everywhere in the code where LED1 is, it will be replaced with P1_0 when compiled.
 * So instead of having to type P1_0 everywhere, you can just use LED1
 *
 *  Created on: Apr 12, 2019
 *      Author: mwrouse
 */
#ifndef INCLUDE_PINS_H_
#define INCLUDE_PINS_H_

/* LEDs (all active HIGH) */
#define LED1    P1_0    /* On the Launchpad */
#define LED2    P4_7    /* On the Launchpad */
#define LED3    P8_1
#define LED4    P2_3
#define LED5    P3_1
#define LED6    P3_0
#define LED7    P7_4
#define LED8    P2_2

/* Switches (all active HIGH) */
#define SWITCH1 P2_1    /* On the Launchpad */
#define SWITCH2 P1_1    /* On the Launchpad */
#define SWITCH3 P1_4
#define SWITCH4 P1_3
#define SWITCH5 P1_2

/* Switch Aliases */
#define SW1     SWITCH1 /* On the Launchpad */
#define SW2     SWITCH2 /* On the Launchpad */
#define SW3     SWITCH3
#define SW4     SWITCH4
#define SW5     SWITCH5

/* Buzzer */
#define BUZZER  P2_0

/* Potentiometer */
#define POT1    P6_1

/* CAN */
#define CAN_SPI_BUS     SPI_BUS_4
#define CAN_MOSI        P4_1
#define CAN_MISO        P4_2
#define CAN_CLK         P4_3
#define CAN_INT         P2_6
#define CAN_CS          P4_0

/* 7-Segment LED */
#define SEVSEG_DATA     P1_5
#define SEVSEG_CLK      P2_4
#define SEVSEG_LATCH    P2_5

/* 7-Segment Aliases */
#define SHIFT_DATA      SEVSEG_DATA
#define SHIFT_CLK       SEVSEG_CLK
#define SHIFT_LATCH     SEVSEG_LATCH

#endif
