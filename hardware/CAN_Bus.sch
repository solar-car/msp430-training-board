EESchema Schematic File Version 4
LIBS:msp430-training-board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR021
U 1 1 59B6B886
P 4200 4650
F 0 "#PWR021" H 4200 4400 50  0001 C CNN
F 1 "GND" H 4200 4500 50  0000 C CNN
F 2 "" H 4200 4650 50  0000 C CNN
F 3 "" H 4200 4650 50  0000 C CNN
	1    4200 4650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR025
U 1 1 59B6B8B2
P 5900 3000
F 0 "#PWR025" H 5900 2850 50  0001 C CNN
F 1 "+3.3V" H 5900 3140 50  0000 C CNN
F 2 "" H 5900 3000 50  0000 C CNN
F 3 "" H 5900 3000 50  0000 C CNN
	1    5900 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3000 5900 3150
Text HLabel 5700 4100 2    60   Input ~ 0
MOSI
Text HLabel 5700 4000 2    60   Input ~ 0
MISO
Text HLabel 5700 3900 2    60   Input ~ 0
~CS
Text HLabel 5700 4300 2    60   Input ~ 0
SCK
Text HLabel 5700 4400 2    60   Output ~ 0
~CAN_INT
Wire Wire Line
	3600 4400 3600 4300
Wire Wire Line
	3600 4300 3350 4300
Connection ~ 3350 4300
Wire Wire Line
	2700 4300 2600 4300
Wire Wire Line
	2600 4300 2600 4600
Wire Wire Line
	2700 4600 2600 4600
Connection ~ 2600 4600
$Comp
L power:GND #PWR018
U 1 1 59BDB14E
P 2600 4750
F 0 "#PWR018" H 2600 4500 50  0001 C CNN
F 1 "GND" H 2600 4600 50  0000 C CNN
F 2 "" H 2600 4750 50  0000 C CNN
F 3 "" H 2600 4750 50  0000 C CNN
	1    2600 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 59C1D294
P 5150 3150
F 0 "#PWR022" H 5150 2900 50  0001 C CNN
F 1 "GND" H 5150 3000 50  0000 C CNN
F 2 "" H 5150 3150 50  0000 C CNN
F 3 "" H 5150 3150 50  0000 C CNN
	1    5150 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3150 5900 3150
Connection ~ 5900 3150
Wire Wire Line
	5400 3150 5150 3150
Text HLabel 5700 4500 2    60   Output ~ 0
~RX0BF
Text HLabel 5700 4600 2    60   Output ~ 0
~RX1BF
Text HLabel 4400 4000 0    60   Input ~ 0
~TX0RTS
Text HLabel 4400 4100 0    60   Input ~ 0
~TX1RTS
Text HLabel 4400 4300 0    60   Input ~ 0
~TX2RTS
Wire Wire Line
	3350 4300 3000 4300
Wire Wire Line
	2600 4600 2600 4750
$Comp
L solarcar-ic:MCP2515 U4
U 1 1 5CA8F509
P 5050 4150
F 0 "U4" H 5050 4843 59  0000 C CNN
F 1 "MCP2515" H 5050 4738 59  0000 C CNN
F 2 "solarcar-ic:TSSOP-20_4.4x6.5mm_Pitch0.65mm" H 3350 5400 60  0001 C CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en011733" H 3700 5550 60  0001 C CNN
F 4 "Microchip" H 6600 5450 60  0001 C CNN "Manufacturer"
F 5 "MCP2515-I/ST" H 6550 5350 60  0001 C CNN "Manufacturer Part Number"
F 6 "Digikey" H 6600 5850 60  0001 C CNN "Supplier"
F 7 "MCP2515-I/ST-ND" H 6600 5700 60  0001 C CNN "Supplier Part Number"
	1    5050 4150
	1    0    0    -1  
$EndComp
$Comp
L solarcar-ic:SN65HVD234 U3
U 1 1 5CA8F596
P 5000 2050
F 0 "U3" H 5000 2393 59  0000 C CNN
F 1 "SN65HVD234" H 5000 2288 59  0000 C CNN
F 2 "solarcar-ic:SOIC_5x4mm_Pitch1.27mm" H 3700 3000 60  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn65hvd233.pdf" H 3950 3100 60  0001 C CNN
F 4 "Texas Instruments" H 6250 2750 60  0001 C CNN "Manufacturer"
F 5 "SN65HVD234DR" H 6250 2650 60  0001 C CNN "Manufacturer Part Number"
F 6 "Digikey" H 6250 3050 60  0001 C CNN "Supplier"
F 7 "296-27991-1-ND" H 6250 2950 60  0001 C CNN "Supplier Part Number"
	1    5000 2050
	1    0    0    -1  
$EndComp
$Comp
L solarcar-connectors:Molex_Microfit_6pin_Female P1
U 1 1 5CA8F624
P 7350 1600
F 0 "P1" H 7428 1653 59  0000 L CNN
F 1 "Molex_Microfit_6pin_Female" H 8750 2300 59  0001 C CNN
F 2 "solarcar-connectors:Molex_Microfit3_Header_02x03_Angled" H 6500 3100 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/430450600_sd.pdf" H 6850 3200 60  0001 C CNN
F 4 "Molex" H 8800 2550 60  0001 C CNN "Manufacturer"
F 5 "0430450600" H 8800 2450 60  0001 C CNN "Manufacturer Part Number"
F 6 "Digikey" H 8800 2800 60  0001 C CNN "Supplier"
F 7 "WM1815-ND" H 8800 2700 60  0001 C CNN "Supplier Part Number"
F 8 "Molex_Microfit_6pin_Female" H 7428 1547 60  0000 L CNN "Name"
	1    7350 1600
	1    0    0    -1  
$EndComp
$Comp
L solarcar-connectors:Molex_Microfit_6pin_Female P2
U 1 1 5CA8F6B8
P 7350 2300
F 0 "P2" H 7428 2353 59  0000 L CNN
F 1 "Molex_Microfit_6pin_Female" H 8750 3000 59  0001 C CNN
F 2 "solarcar-connectors:Molex_Microfit3_Header_02x03_Angled" H 6500 3800 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/430450600_sd.pdf" H 6850 3900 60  0001 C CNN
F 4 "Molex" H 8800 3250 60  0001 C CNN "Manufacturer"
F 5 "0430450600" H 8800 3150 60  0001 C CNN "Manufacturer Part Number"
F 6 "Digikey" H 8800 3500 60  0001 C CNN "Supplier"
F 7 "WM1815-ND" H 8800 3400 60  0001 C CNN "Supplier Part Number"
F 8 "Molex_Microfit_6pin_Female" H 7428 2247 60  0000 L CNN "Name"
	1    7350 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 4650 4200 4600
Wire Wire Line
	4200 4600 4400 4600
Wire Wire Line
	5900 3700 5700 3700
Wire Wire Line
	5900 3150 5900 3700
NoConn ~ 4350 4200
Wire Wire Line
	3600 4400 4400 4400
Wire Wire Line
	3600 4500 4400 4500
NoConn ~ 5700 4200
$Comp
L power:+3.3V #PWR026
U 1 1 5CA97760
P 6000 3700
F 0 "#PWR026" H 6000 3550 50  0001 C CNN
F 1 "+3.3V" H 6000 3840 50  0000 C CNN
F 2 "" H 6000 3700 50  0000 C CNN
F 3 "" H 6000 3700 50  0000 C CNN
	1    6000 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3700 6000 3800
Wire Wire Line
	6000 3800 5700 3800
Wire Wire Line
	4400 3800 4300 3800
Wire Wire Line
	4300 3800 4300 2250
Wire Wire Line
	4300 2250 4500 2250
Wire Wire Line
	4500 1950 4350 1950
Wire Wire Line
	4350 1950 4350 3700
Wire Wire Line
	4350 3700 4400 3700
$Comp
L power:+3.3V #PWR020
U 1 1 5CA99BB3
P 4050 2200
F 0 "#PWR020" H 4050 2050 50  0001 C CNN
F 1 "+3.3V" H 4050 2340 50  0000 C CNN
F 2 "" H 4050 2200 50  0000 C CNN
F 3 "" H 4050 2200 50  0000 C CNN
	1    4050 2200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5CA99763
P 4050 1950
F 0 "#PWR019" H 4050 1700 50  0001 C CNN
F 1 "GND" H 4050 1800 50  0000 C CNN
F 2 "" H 4050 1950 50  0000 C CNN
F 3 "" H 4050 1950 50  0000 C CNN
	1    4050 1950
	-1   0    0    1   
$EndComp
Wire Wire Line
	4050 2200 4050 2150
Wire Wire Line
	4050 2150 4500 2150
Wire Wire Line
	4500 2050 4050 2050
Wire Wire Line
	4050 1950 4050 2050
$Comp
L power:+3.3V #PWR023
U 1 1 5CA9B822
P 5600 2350
F 0 "#PWR023" H 5600 2200 50  0001 C CNN
F 1 "+3.3V" H 5600 2490 50  0000 C CNN
F 2 "" H 5600 2350 50  0000 C CNN
F 3 "" H 5600 2350 50  0000 C CNN
	1    5600 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5500 2250 5600 2250
Wire Wire Line
	5600 2250 5600 2350
$Comp
L power:GND #PWR024
U 1 1 5CA9BC83
P 5650 1900
F 0 "#PWR024" H 5650 1650 50  0001 C CNN
F 1 "GND" H 5650 1750 50  0000 C CNN
F 2 "" H 5650 1900 50  0000 C CNN
F 3 "" H 5650 1900 50  0000 C CNN
	1    5650 1900
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 1900 5650 1950
Wire Wire Line
	5650 1950 5500 1950
Wire Wire Line
	5500 2050 6200 2050
Wire Wire Line
	6200 2050 6200 1650
Wire Wire Line
	6200 1650 7200 1650
Wire Wire Line
	7200 2350 6200 2350
Wire Wire Line
	6200 2350 6200 2050
Connection ~ 6200 2050
Wire Wire Line
	5500 2150 6300 2150
Wire Wire Line
	6300 2150 6300 1750
Wire Wire Line
	6300 1750 7200 1750
Wire Wire Line
	7200 2450 6300 2450
Wire Wire Line
	6300 2450 6300 2150
Connection ~ 6300 2150
Wire Wire Line
	7200 1850 7100 1850
Wire Wire Line
	7100 1850 7100 2550
Wire Wire Line
	7100 2550 7200 2550
$Comp
L power:+12V #PWR030
U 1 1 5CAA5481
P 7050 1300
F 0 "#PWR030" H 7050 1150 50  0001 C CNN
F 1 "+12V" H 7065 1473 50  0000 C CNN
F 2 "" H 7050 1300 50  0001 C CNN
F 3 "" H 7050 1300 50  0001 C CNN
	1    7050 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 1300 7050 1350
Wire Wire Line
	7050 1350 7200 1350
$Comp
L power:+12V #PWR028
U 1 1 5CAA5D67
P 6850 2050
F 0 "#PWR028" H 6850 1900 50  0001 C CNN
F 1 "+12V" H 6865 2223 50  0000 C CNN
F 2 "" H 6850 2050 50  0001 C CNN
F 3 "" H 6850 2050 50  0001 C CNN
	1    6850 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 2050 7200 2050
$Comp
L power:GND #PWR029
U 1 1 5CAA66C9
P 6900 1400
F 0 "#PWR029" H 6900 1150 50  0001 C CNN
F 1 "GND" H 6905 1227 50  0000 C CNN
F 2 "" H 6900 1400 50  0001 C CNN
F 3 "" H 6900 1400 50  0001 C CNN
	1    6900 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	6900 1400 6900 1450
Wire Wire Line
	6900 1450 7200 1450
$Comp
L power:GND #PWR027
U 1 1 5CAA70CB
P 6700 2050
F 0 "#PWR027" H 6700 1800 50  0001 C CNN
F 1 "GND" H 6705 1877 50  0000 C CNN
F 2 "" H 6700 2050 50  0001 C CNN
F 3 "" H 6700 2050 50  0001 C CNN
	1    6700 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	6700 2050 6700 2150
Wire Wire Line
	6700 2150 7200 2150
Wire Wire Line
	7200 1550 7050 1550
Wire Wire Line
	7050 1550 7050 2250
Wire Wire Line
	7050 2250 7200 2250
$Comp
L solarcar-device:AMB-3 Y1
U 1 1 5CABF655
P 3350 4500
F 0 "Y1" V 3297 4631 59  0000 L CNN
F 1 "AMB-3" V 3402 4631 59  0000 L CNN
F 2 "solarcar-device:AMB-3" H 2150 5050 60  0001 C CNN
F 3 "http://www.abracon.com/Resonators/abm3.pdf" H 2250 5200 60  0001 C CNN
F 4 "Digikey" H 4250 5300 60  0001 C CNN "Supplier_Name"
F 5 "535-9103-1-ND" H 4250 5200 60  0001 C CNN "Supplier_Part_Number"
F 6 "ABM3-16.000MHZ-B2-T" H 4300 4950 60  0001 C CNN "Manufactuer_Part_Number"
F 7 "Abracon LLC" H 4300 5050 60  0001 C CNN "Manufacturer"
	1    3350 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 4600 3150 4600
Wire Wire Line
	3150 4600 3150 4700
Wire Wire Line
	3150 4700 3350 4700
Wire Wire Line
	3600 4500 3600 4700
Wire Wire Line
	3600 4700 3350 4700
Connection ~ 3350 4700
$Comp
L solarcar-device:Capacitor_Unpolarized C2
U 1 1 5CAC2275
P 2850 4600
F 0 "C2" V 2900 4700 59  0000 C CNN
F 1 "18pF" V 3000 4600 59  0000 C CNN
F 2 "solarcar-device:Capacitor_0805" H 1950 5450 50  0001 C CNN
F 3 "" H 2850 4600 50  0001 C CNN
	1    2850 4600
	0    1    1    0   
$EndComp
$Comp
L solarcar-device:Capacitor_Unpolarized C3
U 1 1 5CAC243C
P 5550 3150
F 0 "C3" V 5600 3250 59  0000 C CNN
F 1 "0.1uF" V 5700 3150 59  0000 C CNN
F 2 "solarcar-device:Capacitor_0805" H 4650 4000 50  0001 C CNN
F 3 "" H 5550 3150 50  0001 C CNN
	1    5550 3150
	0    1    1    0   
$EndComp
$Comp
L solarcar-device:Capacitor_Unpolarized C1
U 1 1 5CAC2D1D
P 2850 4300
F 0 "C1" V 2900 4400 59  0000 C CNN
F 1 "18pF" V 3000 4300 59  0000 C CNN
F 2 "solarcar-device:Capacitor_0805" H 1950 5150 50  0001 C CNN
F 3 "" H 2850 4300 50  0001 C CNN
	1    2850 4300
	0    1    1    0   
$EndComp
$EndSCHEMATC
