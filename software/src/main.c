#include "drivers/library.h"
#include "include/pins.h"

/* Functions */
void setup(void);


/**
 * This is where the program starts, it should run forever with an infinite loop
 */
int main(void)
{
    setup();


    /* Loop forever */
    while (true)
    {
        pin_toggle(LED8);
        pin_toggle(LED7);
        pin_toggle(LED6);
        pin_toggle(LED5);
        pin_toggle(LED4);
        pin_toggle(LED3);
        delay_millis(500);
        pin_toggle(LED1);
        pin_toggle(LED2);

    }
	
	return 0;
}


/**
 * The setup function initializes everything
 */
void setup(void)
{
    watchdog_disable(); /* Prevent MSP430 from resetting */

    /* Configure CAN */
    //spi_setup(CAN_SPI_BUS, CAN_MOSI, CAN_MISO, CAN_CLK);
    //can_accept(...);
    //can_setup(CAN_SPI_BUS, CAN_CS, CAN_INT);

    /* Configure the Pins */
    pin_set_mode(LED1, Output);
    pin_set_mode(LED2, Output);
    pin_set_mode(LED3, Output);
    pin_set_mode(LED4, Output);
    pin_set_mode(LED5, Output);
    pin_set_mode(LED6, Output);
    pin_set_mode(LED7, Output);
    pin_set_mode(LED8, Output);

    pin_set_mode(SWITCH1, Input);
    pin_set_mode(SWITCH2, Input);
    pin_set_mode(SWITCH3, Input);
    pin_set_mode(SWITCH4, Input);
    pin_set_mode(SWITCH5, Input);

    pin_set_mode(BUZZER, Output);

    pin_set_mode(SEVSEG_DATA, Output);
    pin_set_mode(SEVSEG_CLK, Output);
    pin_set_mode(SEVSEG_LATCH, Output);
}
